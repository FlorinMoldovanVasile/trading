// asta face o valoarea medie in USD pentru fiecare coin\an (din 2017 pana in prezent) si le pune intr-un csv

const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();
const csv = require('csv-writer');

const coins=[
      '0x',
      'aelf',
      'Aeron',
      'Algorand',
      'Aion',
      'Augur',
      'Basic-Attention-Token',
      'BinanceCoin',
      'Bitcoin',
      'Bitcoin-Cash',
      'Bitcoin-Gold',
      'bitcoin-cash-sv',
      'bittorrent-2',
      'Cardano',
      'Chainlink',
      'Cosmos',
      'Dash',
      'Decentraland',
      'Decred',
      'Dent',
      'DigiByte',
      'DigixDAO',
      'Dogecoin',
      'Energi',
      'EnjinCoin',
      'Ethereum',
      'Ethereum-Classic',
      'EOS',
      'Golem',
      'Groestlcoin',
      'zencash',
      'Kyber-Network',
      'ICON',
      'Komodo',
      'Lisk',
      'Litecoin',
      'Maker',
      'MonaCoin',
      'Monero',
      'Nano',
      'Numeraire',
      'NEM',
      'Neo',
      'omisego',
      'Ontology',
      // 'PAX-Gold',// E
      // 'Paxos-Standard', //asta-i de ala forte stabil // C E
// 'Polymath',// C E
      'Power-Ledger',
      'PIVX',
      'Ripio-Credit-Network',
// 'STASISEURO',  //asta-i de ala forte stabil // C E
      'Status',
      'Stellar',
      'Stratis',
// 'SynthetixNetworkToken',// C
      'Syscoin',
      'TRON',
      'Tezos',
// 'UNUSSEDLEO',// E
      'VeChain',
      'Verge',
      'Vertcoin',
      'Qtum',
      'WAX',
      'Waves',
      'WhiteCoin',
      'Ripple',
      'Zcash',
      'Zcoin',
];

const boughtOrSoldCoins={
  'Bitcoin':8785.1,
  'Bitcoin-Cash':240.747,
  'bitcoin-cash-sv':195.07,
  'Groestlcoin':0.177307,
  'zencash':6.4298,
  'Litecoin':43.97549329,
  'Maker':465.92,
  'NEM':0.040686,
  'Ripple':0.196261886,
  'Zcash':47.148,
}

const csvWriterAverageValueCoins = [];
const recordsCSVAverageValueCoins = csv.createObjectCsvWriter({
  path: 'averageValueCoins.csv',
  header: [
    {id: 'coins', title: 'coins'},
    {id: 'bought/sold', title: 'bought/sold'},
    {id: 'current', title: 'current'},
    {id: 'overallAvg', title: 'overallAvg'},
    {id: '2020Avg', title: '2020Avg'},
    {id: '2019Avg', title: '2019Avg'},
    {id: '2018Avg', title: '2018Avg'},
    {id: '2017Avg', title: '2017Avg'},
  ]
});

var spinUp = () => {
  coins.forEach((coin, i) => {
    setTimeout(() => {
      CoinGeckoClient.coins.fetchMarketChartRange(coin.toLocaleLowerCase(), {
        from: new Date(2017,01,01).getTime()/1000,
        to: new Date(2018,01,01).getTime()/1000,
        vs_currency:['usd']
      }).then((data)=>{
        let response = data.data.prices;
        let increment = 0;
        let sum = 0;

        response.forEach((responseDay)=>{
          increment++;
          sum+=responseDay[1];
        })
        
        csvWriterAverageValueCoins[i] = {};
        csvWriterAverageValueCoins[i].coins = coin;
        csvWriterAverageValueCoins[i]['2017Avg'] = sum/increment;

        if(boughtOrSoldCoins[coin]){
          csvWriterAverageValueCoins[i]['bought/sold'] = boughtOrSoldCoins[coin];
        }

        console.log(csvWriterAverageValueCoins[i])
      });
    },i*1100)

    setTimeout(() => {
      CoinGeckoClient.coins.fetchMarketChartRange(coin.toLocaleLowerCase(), {
        from: new Date(2018,01,01).getTime()/1000,
        to: new Date(2019,01,01).getTime()/1000,
        vs_currency:['usd']
      }).then((data)=>{
        let response = data.data.prices;
        let increment = 0;
        let sum = 0;

        response.forEach((responseDay)=>{
          increment++;
          sum+=responseDay[1];
        })
        
        csvWriterAverageValueCoins[i]['2018Avg'] = sum/increment;

        console.log(csvWriterAverageValueCoins[i])
      });

    },(coins.length+i)*1100)

    setTimeout(() => {
      CoinGeckoClient.coins.fetchMarketChartRange(coin.toLocaleLowerCase(), {
        from: new Date(2019,01,01).getTime()/1000,
        to: new Date(2020,01,01).getTime()/1000,
        vs_currency:['usd']
      }).then((data)=>{
        let response = data.data.prices;
        let increment = 0;
        let sum = 0;

        response.forEach((responseDay)=>{
          increment++;
          sum+=responseDay[1];
        })
        
        csvWriterAverageValueCoins[i]['2019Avg'] = sum/increment;

        console.log(csvWriterAverageValueCoins[i])
      });

    },(2*coins.length+i)*1100)

    setTimeout(() => {
      CoinGeckoClient.coins.fetchMarketChartRange(coin.toLocaleLowerCase(), {
        from: new Date(2020,01,01).getTime()/1000,
        to: new Date().getTime()/1000,
        vs_currency:['usd']
      }).then((data)=>{
        let response = data.data.prices;
        let increment = 0;
        let sum = 0;

        response.forEach((responseDay)=>{
          increment++;
          sum+=responseDay[1];
        })
        
        csvWriterAverageValueCoins[i]['2020Avg'] = sum/increment;

        console.log(csvWriterAverageValueCoins[i])
      });

    },(3*coins.length+i)*1100)

    setTimeout(() => {
      CoinGeckoClient.coins.fetchMarketChartRange(coin.toLocaleLowerCase(), {
        from: new Date(2017,01,01).getTime()/1000,
        to: new Date().getTime()/1000,
        vs_currency:['usd']
      }).then((data)=>{
        let response = data.data.prices;
        let increment = 0;
        let sum = 0;

        response.forEach((responseDay)=>{
          increment++;
          sum+=responseDay[1];
        })
        
        csvWriterAverageValueCoins[i]['overallAvg'] = sum/increment;
        csvWriterAverageValueCoins[i]['current'] = response[response.length-1][1];

        console.log(csvWriterAverageValueCoins[i])

        if(i+1===coins.length){        
          recordsCSVAverageValueCoins.writeRecords(csvWriterAverageValueCoins)
            .then(() =>{
              console.log('done');
            })
        }
      });

    },(4*coins.length+i)*1100)
  })
}

spinUp();