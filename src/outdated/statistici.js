const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();

const CoinMarketCap = require('coinmarketcap-api')

const coins=[
  '0x',
  'aelf',
  'Aeron',
  'Algorand',
  'Aion',
  'Augur',
  'Basic-Attention-Token',
  'BinanceCoin',
  'Bitcoin',
  'Bitcoin-Cash',
  'Bitcoin-Gold',
  'bitcoin-cash-sv',
  'bittorrent-2',
  'Cardano',
  'Chainlink',
  'Cosmos',
  'Dash',
  'Decentraland',
  'Decred',
  'Dent',
  'DigiByte',
  'DigixDAO',
  'Dogecoin',
  'Energi',
  'EnjinCoin',
  'Ethereum',
  'Ethereum-Classic',
  'EOS',
  'Golem',
  'Groestlcoin',
  'zencash',
  'Kyber-Network',
  'ICON',
  'Komodo',
  'Lisk',
  'Litecoin',
  'Maker',
  'MonaCoin',
  'Monero',
  'Nano',
  'Numeraire',
  'NEM',
  'Neo',
  'omisego',
  'Ontology',
  // 'PAX-Gold',// E
  // 'Paxos-Standard', //asta-i de ala forte stabil // C E
  // 'Polymath',// C E
  'Power-Ledger',
  'PIVX',
  'Ripio-Credit-Network',
  // 'STASISEURO',  //asta-i de ala forte stabil // C E
  'Status',
  'Stellar',
  'Stratis',
  'havven',// C
  'Syscoin',
  'TRON',
  'Tezos',
  'leo-token',// E
  'VeChain',
  'Verge',
  'Vertcoin',
  'Qtum',
  'WAX',
  'Waves',
  'WhiteCoin',
  'Ripple',
  'Zcash',
  'Zcoin',
];

const boughtOrSoldCoins={
  'Bitcoin':{
    'price':9205,
    'amount':0.0445738
  },
  'Bitcoin-Cash':{
    'price':257.9,
    'amount':0.7754671
  },
  'bitcoin-cash-sv':{
    'price':192.44,
    'amount':1.0419697
  },
  'Cosmos':{
    'price':2.809,
    'amount':71.442251
  },
  'EOS':{
    'price':2.75,
    'amount':72.0562
  },
  'Groestlcoin':{
    'price':0.177307,
    'amount':0
  },
  'zencash':{
    'price':6.4298,
    'amount':0
  },
  'Litecoin':{
    'price':46.1,
    'amount':4.3330665
  },
  'Ontology':{
    'price':0.601,
    'amount':331
  },
  'Maker':{
    'price':465.92,
    'amount':0
  },
  'NEM':{
    'price':0.040686,
    'amount':0
  },
  'Ripple':{
    'price':0.2020086519,
    'amount':1004.0015
  },
  'TRON':{
    'price':0.0166344,
    'amount':13259.415
  },
  'Zcash':{
    'price':51.37,
    'amount':3.899878
  }
}

let statsList = [];
let statsListBoughtCoins = [];

const apiKey = 'b2ea0227-6d63-4ba2-a6cf-f45d143b849b'
const client = new CoinMarketCap(apiKey)
const fiat = 'USD';
const coinsCoinMarketCap = [
      '0x',// E
      'aelf',// C
      'Aeron',// C E
      'Algorand',// C
      'Aion',// C
      'Augur',// C E
      'Basic Attention Token',// E
      'Binance Coin',// C E
      'Bitcoin',// C E
      'Bitcoin Cash',// C E
      'Bitcoin Gold',// C E
      'Bitcoin SV',// C E
      'BitTorrent',// E
      'Cardano',// E
      'Chainlink',// E
      'Cosmos',// E
      'Dash',// C E
      'Decentraland',// E
      'Decred',// C E
      'Dent',// C E
      'DigiByte',// C E
      'DigixDAO',// E
      'Dogecoin',// C E
      'Energi',// C
      'Enjin Coin',// C
      'Ethereum',// C E
      'Ethereum Classic',// C E
      'EOS',// C E
      'Golem',// C E
      'Groestlcoin',// C
      'Horizen',// C
      'Kyber Network',// E
      'ICON',// E
      'Komodo',// C
      'Lisk',// E
      'Litecoin',// C E
      'Maker',// C E
			'MonaCoin',// C
      'Monero',// C E
      'Nano',// E
      'Numeraire',// C E
      'NEM',// C E
      'Neo',// E
      'OMG Network',// E
      'Ontology',// E
      // 'PAX Gold',// E
    //  'Paxos Standard', //asta-i de ala forte stabil // C E
      // 'Polymath',// C E
      'Power Ledger',// C E
      'PIVX',// C
      'Ripio Credit Network',// C E
    //  'STASIS EURO',  //asta-i de ala forte stabil // C E
      'Status',// E
      'Stellar',// E
      'Stratis',// C
      'Synthetix Network Token',// C
      'Syscoin',// C
      'TRON',// C E
      'Tezos',// E
      'UNUS SED LEO',// E
      'VeChain',// E
      'Verge',// C
      'Vertcoin',// C
      'Qtum',// C E
      'WAX',// E
      'Waves',// E
      'WhiteCoin',// C
      'XRP',// C E
			'Zcash',// C E
      'Zcoin',// C
]
let currentResults={};

var spinUp = () => {
  client.getTickers({start: 1, limit: 700, convert: fiat})
  .then(coins =>{
    coins.data.forEach((coin) => {
      if(coinsCoinMarketCap.indexOf(coin.name)>=0){
        currentResults[coin.name] = coin.quote[fiat].price
      }
    })
  })
  .then(()=>{
    coins.forEach((coin, i) => {
      setTimeout(() => {
        CoinGeckoClient.coins.fetchMarketChartRange(coin.toLocaleLowerCase(), {
          from: new Date(2020,04,01).getTime()/1000,
          to: new Date().getTime()/1000,
          vs_currency:['usd']
        }).then((data)=>{
          let response = data.data.prices;
          let increment = 0;
          let sum = 0;
  
          response.forEach((responseDay)=>{
            increment++;
            sum+=responseDay[1];
          })
  
          let averageResult = sum/increment;
          
          let temporaryCoin = {
              coin:coin,
              current:currentResults[coinsCoinMarketCap[i]],
              average:averageResult,
              currentStats: ((currentResults[coinsCoinMarketCap[i]] * 100) / averageResult) - 100
          };
          
          if(
            Object.keys(boughtOrSoldCoins).indexOf(coin)>=0 && 
            boughtOrSoldCoins[coin].amount>0
          ){
            statsListBoughtCoins.push({
              coin:coin,
              current:currentResults[coinsCoinMarketCap[i]],
              average:averageResult,
              bought:boughtOrSoldCoins[coin],
              currentStats:((currentResults[coinsCoinMarketCap[i]] * 100) / boughtOrSoldCoins[coin].price) - 100
            })
          }

          statsList.push(temporaryCoin);
  
          if(i === coins.length-1){
            console.log(statsList.sort(compare))
            console.log('=======================================');
            console.log(statsListBoughtCoins.sort(compare))
          }
        });
  
      },i*1100)
    })
  })
}

spinUp();


function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const bandA = a.currentStats;
  const bandB = b.currentStats;

  let comparison = 0;
  if (bandA > bandB) {
    comparison = -1;
  } else if (bandA < bandB) {
    comparison = 1;
  }
  return comparison;
}