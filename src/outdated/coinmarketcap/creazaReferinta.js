// asta face un csv cu primele 100 de coinuri ordonate dupa marketcap
// pentru fiecare coin, avem numele, rankul si pretul
const CoinMarketCap = require('coinmarketcap-api')
const csv = require('csv-writer');

const apiKey = 'b2ea0227-6d63-4ba2-a6cf-f45d143b849b'
const client = new CoinMarketCap(apiKey)

const fiat = 'USD';

let recordsCSVTop100Coins = [];
const csvWriterTop100Coins = csv.createObjectCsvWriter({
    path: 'top100coins.csv',
    header: [
        {id: 'coins', title: 'coins'},
        {id: 'rank', title: 'rank'},
        {id: 'price', title: 'price'},
    ]
});

client.getTickers({start: 1, limit: 100, convert: fiat})
    .then(coins =>{
        coins.data.forEach((coin, index) => {
            recordsCSVTop100Coins[index] = {}
            recordsCSVTop100Coins[index].coins = coin.name;
            recordsCSVTop100Coins[index]['rank'] = coin.cmc_rank;
            recordsCSVTop100Coins[index]['price'] = coin.quote[fiat].price;
        });
    })
    .then(() => csvWriterTop100Coins.writeRecords(recordsCSVTop100Coins))
    .then(() =>{
        console.log('done');
    })
