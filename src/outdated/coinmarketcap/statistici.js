//asta zice schimbarea in % intre coinurile dintr-o refereinta
//si coinurile din momentul actual

const CoinMarketCap = require('coinmarketcap-api')
const csv = require('csv-parser')
const fs = require('fs')

const apiKey = 'b2ea0227-6d63-4ba2-a6cf-f45d143b849b'
const client = new CoinMarketCap(apiKey)

const fiat = 'USD';
const bestCoins = [
      '0x',// E
      'aelf',// C
      'Aeron',// C E
      'Algorand',// C
      'Aion',// C
      'Augur',// C E
      'Basic Attention Token',// E
      'Binance Coin',// C E
      'Bitcoin',// C E
      'Bitcoin Cash',// C E
      'Bitcoin Gold',// C E
      'Bitcoin SV',// C E
      'BitTorrent',// E
      'Cardano',// E
      'Chainlink',// E
      'Cosmos',// E
      'Dash',// C E
      'Decentraland',// E
      'Decred',// C E
      'Dent',// C E
      'DigiByte',// C E
      'DigixDAO',// E
      'Dogecoin',// C E
      'Energi',// C
      'Enjin Coin',// C
      'Ethereum',// C E
      'Ethereum Classic',// C E
      'EOS',// C E
      'Golem',// C E
      'Groestlcoin',// C
      'Horizen',// C
      'Kyber Network',// E
      'ICON',// E
      'Komodo',// C
      'Lisk',// E
      'Litecoin',// C E
      'Maker',// C E
			'MonaCoin',// C
      'Monero',// C E
      'Nano',// E
      'Numeraire',// C E
      'NEM',// C E
      'Neo',// E
      'OMG Network',// E
      'Ontology',// E
      'PAX Gold',// E
    //  'Paxos Standard', //asta-i de ala forte stabil // C E
      'Polymath',// C E
      'Power Ledger',// C E
      'PIVX',// C
      'Ripio Credit Network',// C E
    //  'STASIS EURO',  //asta-i de ala forte stabil // C E
      'Status',// E
      'Stellar',// E
      'Stratis',// C
      'Synthetix Network Token',// C
      'Syscoin',// C
      'TRON',// C E
      'Tezos',// E
      'UNUS SED LEO',// E
      'VeChain',// E
      'Verge',// C
      'Vertcoin',// C
      'Qtum',// C E
      'WAX',// E
      'Waves',// E
      'WhiteCoin',// C
      'XRP',// C E
			'Zcash',// C E
      'Zcoin',// C
]
const myCoins = [
  'Bitcoin Cash',
  'Bitcoin SV',
  'Litecoin',
  'XRP',
  'Zcash'
]

const referenceResults = {};
const newResults = {};
const stats = {};
const statsList = [];
 
fs.createReadStream('referinta.csv')
.pipe(csv())
.on('data', (data) => {
  referenceResults[data.Coins] = parseFloat(data.Price);
})
.on('end', () => {
  client.getTickers({start: 1, limit: 700, convert: fiat})
  .then(coins =>{
    coins.data.forEach((coin) => {
      newResults[coin.name] = coin.quote[fiat].price
    })
  })
  .then(()=>checkLosers(referenceResults,newResults))
  .then(()=>checkStats(referenceResults,newResults))
  .then((data)=>{
    let i=1;
    Object.keys(data).forEach((coin)=>{
      if(bestCoins.indexOf(coin)>=0){
        let temporaryCoin = {
          coin:coin,
          referenceResults:referenceResults[coin],
          newResults:newResults[coin],
          stats:data[coin],
          myCoin:false,
        };
        if(myCoins.indexOf(coin)>=0){
          temporaryCoin.myCoin = true;
        }
        
        statsList.push(temporaryCoin)
        // console.log(i++,'',coin,': ',referenceResults[coin],'...',newResults[coin],'===>>>',data[coin]);
      }

    })
    console.log(statsList.sort(compare))
  })
});

function checkStats(referenceResultsParam, newResultsParam){
  Object.keys(newResultsParam).forEach((coin)=>{
    if(!referenceResultsParam[coin]){
      // console.log('new coin in town:',coin);
    }else{
      stats[coin] = ((newResultsParam[coin] * 100) / referenceResultsParam[coin]) - 100;
    }
  })
  return stats;
}

function checkLosers(referenceResultsParam, newResultsParam){
  Object.keys(referenceResultsParam).forEach((coin)=>{
    if(!newResultsParam[coin]){
      console.log('loser:',coin);
    }
  })
  return;
}

function compare(a, b) {
  // Use toUpperCase() to ignore character casing
  const bandA = a.stats;
  const bandB = b.stats;

  let comparison = 0;
  if (bandA > bandB) {
    comparison = -1;
  } else if (bandA < bandB) {
    comparison = 1;
  }
  return comparison;
}