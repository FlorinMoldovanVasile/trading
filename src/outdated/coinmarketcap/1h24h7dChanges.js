//asta face 2 csv-uri, unul cu primele 100 coinuri si altul cu bestCoins ordonate dupa marketcap
//pentru fiecare coin avem numele, rankul si schimbarea pe 1h/24h/7d

var cron = require('node-cron');
const CoinMarketCap = require('coinmarketcap-api')
const csv = require('csv-writer');

const apiKey = 'b2ea0227-6d63-4ba2-a6cf-f45d143b849b'
const client = new CoinMarketCap(apiKey)

const fiat = 'USD';

// cron.schedule('0 0-23 * * *', () => {
    let recordsCSVTop100Coins = [];
    const date = new Date();
    const formatDate = date.getFullYear() + '.' + (date.getMonth()+1) + '.' + date.getDate() + '-' + date.getHours() 
    const csvWriterTop100Coins = csv.createObjectCsvWriter({
        path: 'top100coins//' + formatDate + '.csv',
        header: [
            {id: 'coins', title: 'Coins'},
            {id: 'rank', title: 'Rank'},
            {id: 'price', title: 'Price'},
            {id: '1hourChange', title: '1hourChange'},
            {id: '1dayChange', title: '1dayChange'},
            {id: '7daysChange', title: '7daysChange'}
        ]
    });

    client.getTickers({start: 1, limit: 100, convert: fiat})
        .then(coins =>{
            coins.data.forEach((coin, index) => {
                recordsCSVTop100Coins[index] = {}
                recordsCSVTop100Coins[index].coins = coin.name;
                recordsCSVTop100Coins[index]['rank'] = coin.cmc_rank;
                recordsCSVTop100Coins[index]['price'] = coin.quote[fiat].price;
                recordsCSVTop100Coins[index]['1hourChange'] = coin.quote[fiat].percent_change_1h;
                recordsCSVTop100Coins[index]['1dayChange'] = coin.quote[fiat].percent_change_24h;
                recordsCSVTop100Coins[index]['7daysChange'] = coin.quote[fiat].percent_change_7d;
            });
        })
        .then(() => csvWriterTop100Coins.writeRecords(recordsCSVTop100Coins))
        .then(() =>{
            console.log('done');
        })
// });
