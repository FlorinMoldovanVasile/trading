const CoinMarketCap = require('coinmarketcap-api')
const apiKey = 'b2ea0227-6d63-4ba2-a6cf-f45d143b849b'
const client = new CoinMarketCap(apiKey)
const fs = require('fs');
const { table } = require('table');
const myCoins = require('./myCoins.json');

const totalProfitPerCoin = {};
const profit = {};
let resultsForTable = [];
const rawdata = fs.readFileSync('./history/historyProfit.json');
const history = JSON.parse(rawdata);
const currency = 'USD';
const cMALimit = 700;

client.getTickers({start: 1, limit: cMALimit, convert: currency})
  .then((coins) =>{
    resultsForTable = [['from', 'to', 'profitRate']];

    history.forEach((trade) => {
        resultsForTable.push([trade.fromCoin, trade.toCoin, trade.profitRate])

        if(Object.keys(totalProfitPerCoin).indexOf(trade.fromCoin)>=0){
            delete totalProfitPerCoin[trade.fromCoin];
        }
        
        if(totalProfitPerCoin[trade.toCoin] !== undefined){
            totalProfitPerCoin[trade.toCoin] += trade.amountOfToCoin * trade.profitRate / 100;
        }else{
            totalProfitPerCoin[trade.toCoin] = trade.amountOfToCoin * trade.profitRate / 100;
        }
    });

    console.log(
        table(resultsForTable)
    );

    coins.data.forEach((coin) => {
        if(
            Object.keys(totalProfitPerCoin).indexOf(coin.name)>=0 &&
            Object.keys(myCoins).indexOf(coin.name)>=0
        ){

            profit[coin.name] = {
                coinsProfit : totalProfitPerCoin[coin.name],
                [currency] : coin.quote[currency].price
            }
        }
    })
    
    console.log(profit)
  })