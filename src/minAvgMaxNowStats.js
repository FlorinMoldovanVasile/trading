const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();
const CoinMarketCap = require('coinmarketcap-api')
const apiKey = 'b2ea0227-6d63-4ba2-a6cf-f45d143b849b'
const client = new CoinMarketCap(apiKey)
const coins = require('./coinsCoinGeckoMap.json')
const table = require('table');

const fromDate = new Date(2020,5,01).getTime()/1000; //2 month old
const toDate = new Date().getTime()/1000;
const currency = 'usd';
const cMALimit = 5000;
const marketCap = {};
const query = {
    from: fromDate,
    to: toDate,
    vs_currency:[currency]
};
const results = [['coins','min','avg','max','recent','recentVsAvg %']];

client.getTickers({start: 1, limit: cMALimit, convert: currency})
      .then((newValues) =>{
        newValues.data.forEach((newValue) => {
          if(Object.keys(coins).indexOf(newValue.name)>=0){
            marketCap[newValue.name] = newValue.cmc_rank
          }
        })
      }).then(()=>{
        Object.keys(coins).forEach((coin, i) => {
            setTimeout(() => {
              CoinGeckoClient.coins.fetchMarketChartRange(coins[coin], query)
                .then((data)=>{
                    const response = data.data.prices;
                    if(marketCap[coin]<200){
                        results.push(getMinAvgMax(marketCap[coin] + '.' + coin,response));
                    }
        
                    if(i === Object.keys(coins).length-1){
                        CoinGeckoClient.simple.price({
                            ids:Object.values(coins),
                            vs_currencies:currency,
                        }).then((currentData)=>{
                            const currentResponse = currentData.data; 
                            results.forEach((result,index)=>{
                                if(index === 0){
                                    return;
                                };
                                
                                const resultWithoutCMA = (result[0].replace(result[0].substring(0,result[0].indexOf('.')), '')).slice(1);
                                const coinGeckoCoin = coins[resultWithoutCMA]; 
                                const currentValue = currentResponse[coinGeckoCoin][currency];
        
                                results[index].push(currentValue);
                                results[index].push(parseInt((((currentValue * 100) / results[index][2]) - 100),10));
                            })
        
                            const ascResults = results.sort(compare);
                            console.log(table.table(ascResults));
                            console.log(new Date(fromDate*1000),'->',new Date(toDate*1000));
                        })
                    }
                });
        
            },i*1100)
        })
      })


function getMinAvgMax(coin,params){
    let increment = 0;
    let min = 0;
    let max = 0;
    let sum = 0;
    let avg = 0;

    params.forEach((responseDay)=>{
        if(increment === 0){
            min = max = responseDay[1];
        }
        if(min>responseDay[1]){
            min = responseDay[1];
        }
        if(max<responseDay[1]){
            max = responseDay[1];
        }

        increment++;
        sum+=responseDay[1];
    })
    avg = sum/increment;

    return [coin, min.toFixed(7), avg.toFixed(7), max.toFixed(7)];
}

function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const bandA = a[5];
    const bandB = b[5];
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = -1;
    } else if (bandA < bandB) {
      comparison = 1;
    }
    return comparison;
}