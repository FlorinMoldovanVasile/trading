const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();
const CoinMarketCap = require('coinmarketcap-api')
const apiKey = 'b2ea0227-6d63-4ba2-a6cf-f45d143b849b'
const client = new CoinMarketCap(apiKey)
const fs = require('fs');
const coins = require('./coinsCoinGeckoMap.json')
const myCoins = require('./myCoins.json');
const { table } = require('table');

const topTrades = 15;
const currency = 'usd';
const cMALimit = 5000;
const currentResults = {};
const marketCap = {};
const resultsForTable = {};

CoinGeckoClient.simple.price({ ids: Object.values(coins), vs_currencies: currency})
  .then((data)=>{
    Object.keys(data.data).forEach((coin)=>{
      currentResults[Object.keys(coins).find(key => coins[key] === coin)] = data.data[coin][currency];
    })
  }).then(()=>{
    client.getTickers({start: 1, limit: cMALimit, convert: currency})
      .then((newValues) =>{
        newValues.data.forEach((newValue) => {
          if(Object.keys(coins).indexOf(newValue.name)>=0){
            marketCap[newValue.name] = newValue.cmc_rank
          }
        })
      })
      .then(()=>{
        Object.keys(myCoins).forEach((myCoin)=>{
            const rawdata = fs.readFileSync('./history/' + myCoin + '.json');
            const history = JSON.parse(rawdata);
  
            resultsForTable[myCoin] = [[myCoin + "-" + history.date,'old','new','stats']];
  
            Object.keys(history[myCoin]).forEach((historyCoin) => {
                const oldStats = history[myCoin][myCoin] / history[myCoin][historyCoin];
                const newStats = currentResults[myCoin] / currentResults[historyCoin];
                const oldVsNewStats = ((newStats * 100) / oldStats) - 100;
                
                if(marketCap[historyCoin] < 200){
                  resultsForTable[myCoin].push([marketCap[historyCoin] + '.' + historyCoin, oldStats, newStats, oldVsNewStats])
                }
            })
  
            resultsForTable[myCoin] = resultsForTable[myCoin].sort(compare);
            resultsForTable[myCoin] = resultsForTable[myCoin].slice(0, topTrades);
  
            console.log(
                table(
                    resultsForTable[myCoin]
                )
            );
        })
      })
  })

  function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const bandA = a[3];
    const bandB = b[3];
  
    let comparison = 0;
    if (bandA > bandB) {
      comparison = -1;
    } else if (bandA < bandB) {
      comparison = 1;
    }
    return comparison;
  }