const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();
const coins = require('./coinsCoinGeckoMap.json')
const myCoins = require('./myCoins.json');
const fs = require('fs');

const referenceCoin = 'Zcash';
const delay = 1000;
const currency = 'usd';
const query = {
    from: null,
    to: null,
    vs_currency:[currency]
};

const results = {};
results[referenceCoin] = {};

Object.keys(coins).forEach((coin, index) => {
    setTimeout(() => {
        const date = new Date(myCoins[referenceCoin]);
        query.from = date.getTime() /1000;
        query.to = (date.getTime() + 2*60*60*1000) /1000;

        CoinGeckoClient.coins.fetchMarketChartRange(coins[coin], query)
            .then((data)=>{
                results[referenceCoin][coin] = data.data.prices[0][1];
                
                console.log(coin)
                
                if(index + 1 === Object.keys(coins).length){
                    results.date = date

                    const path = './history/' + referenceCoin + '.json';
                    fs.writeFileSync(path, JSON.stringify(results));
                }
            });

    },index*delay)
})